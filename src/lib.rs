use rand::prelude::*;

pub fn bprintln(solution: String) {
    let time = std::time::Duration::from_millis(5);
    let mut tmp = [0; 4];
    let mut i: usize = 0;
    let mut start = "".to_string();
    let alphabet = vec![
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z", " ", "A", "B", "C", "D", "E", "F", "G", "H", "I",
        "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "!",
        "@", "#", "$", "%", "^", "/", "&", "*", "(", ")", "-", "_", "=", "+", ".", ",", "?", "~",
        "`", "|", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
    ];
    let mut rng = thread_rng();
    while start != solution {
        let holder = alphabet.choose(&mut rng).unwrap();
        if holder != &solution.chars().nth(i).unwrap().encode_utf8(&mut tmp) {
            println!("{}{}", start, holder);
            std::thread::sleep(time);
        } else {
            start = format!("{}{}", start, holder);
            println!("{}", start);
            i += 1;
            std::thread::sleep(time);
        };
    }
}

